# MAC0110 - MiniEP7
# Antonio Marcos Shiro Arnauts Hachisuca - 11796041

# A parte1 apresenta as funcoes seno, cosseno e tangente

# A parte2 apresenta funcoes que verificam se um valor x equivale ao valor do seno, cosseno ou tangente de um angulo y

function sin(x)
	n = 1
	y = 0
	sinal = 1
	while n < 13
		y = y + sinal * (x ^ (2 * n - 1)) / factorial(big(2 * n - 1))
		sinal = sinal * -1
		n = n + 1	
	end
	y = round(y, digits = 3)
	return y
end

function cos(x)
	sinal = 1
	y = 0
	n = 0
	while n < 11
		y = y + sinal * (x ^ (2 * n) ) / factorial(big(2 * n))
		sinal = sinal * -1
		n = n + 1
	end
	y = round(y, digits = 3)
	return y
end

function tan(x)
	n = 1
	y = 0
	while n < 12
		y = y - (-1 ^ (3n - 1)) * bernoulli(n) * (2 ^ (2 * n)) * (2 ^ (2 * n) - 1) * (x ^ (2 * n - 1)) / factorial(big(2 * n))
		n = n + 1
	end
	y = round(y, digits = 3)
	return y
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end


function check_sin(x, y)
	return quaseigual(x, sin(y))
end

function check_cos(x, y)
	return quaseigual(x, cos(y))
end

function check_tan(x, y)
	return quaseigual(x, tan(y))
end

function quaseigual(x, y)
	if abs(x - y) <= 0.001
		return true
	else
		return false
	end
end

function taylor_sin(x)
	n = 1
	y = 0
	sinal = 1
	while n < 13
		y = y + sinal * (x ^ (2 * n - 1)) / factorial(big(2 * n - 1))
		sinal = sinal * -1
		n = n + 1	
	end
	y = round(y, digits = 3)
	return y
end

function taylor_cos(x)
	sinal = 1
	y = 0
	n = 0
	while n < 11
		y = y + sinal * (x ^ (2 * n) ) / factorial(big(2 * n))
		sinal = sinal * -1
		n = n + 1
	end
	y = round(y, digits = 3)
	return y
end

function taylor_tan(x)
	n = 1
	y = 0
	while n < 12
		y = y - (-1 ^ (3n - 1)) * bernoulli(n) * (2 ^ (2 * n)) * (2 ^ (2 * n) - 1) * (x ^ (2 * n - 1)) / factorial(big(2 * n))
		n = n + 1
	end
	y = round(y, digits = 3)
	return y
end

function teste()
	if check_sin(0.5, pi / 6) != true || check_sin(1, pi / 2) != true || check_sin(0, 0) != true || check_sin(0, pi) != true
		println("A funcao seno esta incorreta!")
	end
	if check_cos(0.5, pi / 3) != true || check_cos(1, 0) != true || check_cos(0, pi / 2) != true || check_cos(-1, pi) != true
		println("A funcao cosseno esta incorreta!")
	end
	if check_tan(0, 0) != true || check_tan(1, pi / 4) != true || check_tan(0.5773, pi / 6) != true || check_tan(0.4815, pi / 7) != true
		println("A funcao tangente esta incorreta!")
	else
		println("Tudo certo!")
	end
end

teste()
